package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("prize_win")
public class PrizeWin implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id自增
	 */
	@TableId
	private Long id;
	/**
	 * 会员ID
	 */
	private Long memberId;
	/**
	 * 奖品id
	 */
	private Long prizeId;
	/**
	 * 状态：0-未发放 1-已发放
	 */
	private Integer statusId;
	/**
	 * 中奖时间
	 */
	private Date winTime;
	/**
	 * 奖品发放时间
	 */
	private Date grantTime;

}
